//
//  ProgramViewController.swift
//  iamu
//
//  Created by Muhammad Iqbal on 18/04/2018.
//  Copyright © 2018 Muhammad Iqbal. All rights reserved.
//

import UIKit

class ProgramViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = UIColor(red: 1, green: 0.56, blue: 0.27, alpha: 1)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        Helper.helper.setupLeftNavItem(navigationItem: navigationItem)
        Helper.helper.setupRightNavItems(navigationItem: navigationItem)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
