//
//  ProfileViewController.swift
//  iamu
//
//  Created by Muhammad Iqbal on 15/04/2018.
//  Copyright © 2018 Muhammad Iqbal. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import FirebaseFirestore
class ProfileViewController: UIViewController, UITextFieldDelegate {

    
    let db = Firestore.firestore()
    var docRef : DocumentReference!
    let user = Auth.auth().currentUser
  
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var mobileTextField: UITextField!
    

    
    
    
    @IBOutlet weak var simpleSwitch: UISwitchExt!
    
    @IBAction func switchProfile(_ sender: Any) {
        
        if (sender as AnyObject).isOn == true {

                    Helper.helper.switchToNavigationViewController(Navigation: "businessprofilepage")
                }
                
            }
            

    
    override func viewWillAppear(_ animated: Bool) {
        getData()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 1, green: 0.56, blue: 0.27, alpha: 1)
        nameTextField.font = UIFont(name: "Roboto-Regular", size: 12)
        emailTextField.font = UIFont(name: "Roboto-Regular", size: 12)
        mobileTextField.font = UIFont(name: "Roboto-Regular", size: 12)
        
        nameTextField.delegate = self
        emailTextField.delegate = self
        mobileTextField.delegate = self
        
        let emailUser = user!.email
        docRef = Firestore.firestore().document("User/\(emailUser ?? "")")
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 1, green: 0.56, blue: 0.27, alpha: 1)
        simpleSwitch.layer.cornerRadius = simpleSwitch.frame.height / 2
        Helper.helper.setupLeftNavItem(navigationItem: navigationItem)
        Helper.helper.setupRightNavItems(navigationItem: navigationItem)
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func saveButtonDidTapped(_ sender: Any) {
        
        guard
            let name = nameTextField.text,
            name != "",
            let mobile = mobileTextField.text,
            mobile != "",
            let email = emailTextField.text,
            email != ""
            
            else {
                Helper.showAlert(self, title: "Missing Info", message: "Please fill out all fields")
                return
        }
        
        let emailUser = user!.email
        docRef = Firestore.firestore().document("User/\(emailUser ?? "")")
        
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                
                self.db.collection("User").document("\(emailUser ?? "")").updateData([
                    "email": email,
                    "full_Name": name,
                    "mobile_number": mobile,
                    "status": "updated"
                ]) { err in
                    if let err = err {
                        print("Error updating document: \(err)")
                    } else {
                        UserDefaults.standard.set("Key", forKey: "Key")
                        Helper.showAlert(self, title: "Successful", message: "Your profile has been successfully updated")
                        print("Document successfully updated")
                    }
                }
                
            } else {
                self.db.collection("User").document("\(emailUser ?? "")").setData([
                    "email": email,
                    "full_Name": name,
                    "mobile_number": mobile,
                    "status": "updated"
                ]) { err in
                    if let err = err {
                        print("Error updating document: \(err)")
                    } else {
                        UserDefaults.standard.set("Key", forKey: "Key")
                        Helper.showAlert(self, title: "Successful", message: "You have succesfully update your profile")
                        print("Document successfully updated")
                    }
                }
                print("Document does not exist")
            }
        }
        
        
    }
    
    
    @IBAction func logoutButtonDidTapped(_ sender: Any) {
        
        do {
            try Auth.auth().signOut()
            
            Helper.helper.switchToViewController(Navigation: "loginpage")
            
        } catch {
            print(error)
        }
        
    }
    
    func getData () {
        
        docRef.getDocument { (docSnapshot, error) in
            guard let docSnapshot = docSnapshot, docSnapshot.exists else { return }
            let data = docSnapshot.data()
            let email = data!["email"] as? String ?? ""
            let fullName = data!["full_Name"] as? String ?? ""
            let mobileNumber = data!["mobile_number"] as? String ?? ""
            
            self.emailTextField.text! = "\(email)"
            self.nameTextField.text! = fullName
            self.mobileTextField.text! = mobileNumber
            
        }
        
    }
    


}

