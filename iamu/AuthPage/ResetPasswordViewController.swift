//
//  ResetPasswordViewController.swift
//  iamu
//
//  Created by Muhammad Iqbal on 01/05/2018.
//  Copyright © 2018 Muhammad Iqbal. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class ResetPasswordViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailAddressTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        if emailAddressTextField != nil {
            emailAddressTextField.delegate = self
            emailAddressTextField.font = UIFont(name: "Roboto-Regular", size: 12)
        } else {}
        
   
        // Do any additional setup after loading the view.
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    @IBAction func backButtonDidTapped(_ sender: Any) {
        Helper.helper.switchToViewController(Navigation: "loginpage")
    }
    @IBAction func secondBackButtonDidTapped(_ sender: Any) {
        Helper.helper.switchToViewController(Navigation: "loginpage")
    }
    
    @IBAction func resetButtonDidTapped(_ sender: Any) {
        
        guard
            let resetEmail = emailAddressTextField.text,
            resetEmail != ""
            else {
                Helper.showAlert(self, title: "Missing Email Address", message: "Please fill out your email address!")
                return
        }
        
        Auth.auth().sendPasswordReset(withEmail: resetEmail, completion: { (error) in
            //Make sure you execute the following code on the main queue
            DispatchQueue.main.async {
                //Use "if let" to access the error, if it is non-nil
                if let error = error {
                    Helper.showAlert(self, title: "Reset Failed", message: error.localizedDescription)
                } else {
                    Helper.helper.switchToViewController(Navigation: "verifiedpassword")
                }
            }
        })

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
