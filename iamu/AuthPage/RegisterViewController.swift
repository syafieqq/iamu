//
//  RegisterViewController.swift
//  iamu
//
//  Created by Muhammad Iqbal on 21/04/2018.
//  Copyright © 2018 Muhammad Iqbal. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseFirestore


class RegisterViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var fullNameTExtField: UITextField!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    let db = Firestore.firestore()
    override func viewDidLoad() {
        super.viewDidLoad()
        fullNameTExtField.delegate = self
        mobileNumberTextField.delegate = self
        emailTextField.delegate = self
        passwordTextField.delegate = self
        
        fullNameTExtField.font = UIFont(name: "Roboto-Regular", size: 12)
         mobileNumberTextField.font = UIFont(name: "Roboto-Regular", size: 12)
        emailTextField.font = UIFont(name: "Roboto-Regular", size: 12)
        passwordTextField.font = UIFont(name: "Roboto-Regular", size: 12)
        
        // Do any additional setup after loading the view.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }

    @IBAction func backButtonDidTapped(_ sender: Any) {
        Helper.helper.switchToViewController(Navigation: "loginpage")
    }
    
    @IBAction func createAccountButtonDidTapped(_ sender: Any) {
        

        guard
            let name = fullNameTExtField.text,
            name != "",
            let mobile = mobileNumberTextField.text,
            mobile != "",
            let email = emailTextField.text,
            email != "",
            let password = passwordTextField.text,
            password != ""
            else {
                Helper.showAlert(self, title: "Missing Info", message: "Please fill out all fields")
                return
        }
        
        Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
            
            guard error == nil else {
                Helper.showAlert(self, title: "Error", message: error!.localizedDescription)
                return
            }
           // guard let user = user else { return }
//            print(user.email ?? "MISSING EMAIL")
//            print(user.uid)
            
        
                
                //MARK Create User Profile realtime database
//                let databaseRef = Database.database().reference()
//                let usersRef = databaseRef.child("Users").child(user.uid)
//
//            let userValue = ["fullname":name, "phonenumber":mobile, "email":email]
//
//                usersRef.updateChildValues(userValue, withCompletionBlock: { (err, ref) in
//
//                    if err != nil {
//                        print(err!.localizedDescription)
//                        return
//                    }
//
//                    Helper.helper.switchToViewController(Navigation: "loginpage")
//                })
            
            //MARK Create User Profile realtime database
            
            // Create an initial document to update.
            let frankDocRef = self.db.collection("User").document("\(email)")
            frankDocRef.setData([
                "email": email,
                "full_Name": name,
                "mobile_number": mobile,
                //"password": password
                "status": "not update"
                ])
            Helper.helper.switchToViewController(Navigation: "loginpage")
            
            // To update age and favorite color:
//            self.db.collection("User").document("frank").updateData([
//            "email": email,
//            "full_Name": name,
//            "mobile_number": mobile,
//            "password": password
//            ]) { err in
//                if let err = err {
//                    print("Error updating document: \(err)")
//                } else {
//                    print("Document successfully updated")
//                }
//            }
            
           
                
            })
     
    }
    
}
