//
//  ViewController.swift
//  iamu
//
//  Created by Muhammad Iqbal on 14/04/2018.
//  Copyright © 2018 Muhammad Iqbal. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import FBSDKLoginKit

class ViewController: UIViewController, UITextFieldDelegate {

//    var programStorage = [DisplayModel4]()
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var newUsernameTextField: UITextField!
    
    @IBOutlet weak var newPasswordTextField: UITextField!
    
    @IBAction func createAccountButtonDidTapped(_ sender: Any) {
        Helper.helper.switchToViewController(Navigation: "signup")
    }
    @IBAction func forgotPasswordButtonDidTapped(_ sender: Any) {
        Helper.helper.switchToViewController(Navigation: "resetpassword")
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     
        newUsernameTextField.font = UIFont(name: "Roboto-Regular", size: 12)
        newPasswordTextField.font = UIFont(name: "Roboto-Regular", size: 12)

        
        
        if (Auth.auth().currentUser != nil) {
            Helper.helper.switchToNavigationViewController(Navigation: "homepage")
        }
        newUsernameTextField.delegate = self
        newPasswordTextField.delegate = self
        cardView.layer.cornerRadius = 5
        Helper.helper.shadow(myView: cardView)

    }
    
    
    @IBAction func loginButtonDidTapped(_ sender: Any) {
        
        guard let email = newUsernameTextField.text,
            email != "",
            let password = newPasswordTextField.text,
            password != ""
            else {
                Helper.showAlert(self, title: "Missing Info", message: "Please fill out all required fields")
                return
        }
        


        
        Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
            if error == nil {
                
                //Print into the console if successfully logged in
                print("You have successfully logged in")
                
//                if UserDefaults.standard.value(forKey:"data") == nil {
//                    Helper.helper.test(vc: self)
//                    self.keepData()
//                    Helper.helper.switchToNavigationViewController(Navigation: "homepage")
//                } else {
                    Helper.helper.switchToNavigationViewController(Navigation: "homepage")
//                }
                //Go to the HomeViewController if the login is sucessful
                
                
            } else {
                
                //Tells the user that there is an error and then gets firebase to tell them the error
                Helper.showAlert(self, title: "Error", message: error!.localizedDescription)
                return

            }
            
          
            
            
        })
        
    }
    
//    @IBAction func fbLoginButtonDidTapped(_ sender: Any) {
//
//        let fbLoginManager = FBSDKLoginManager()
//        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) in
//            if let error = error {
//                print("Failed to login: \(error.localizedDescription)")
//                return
//            }
//
//            guard let accessToken = FBSDKAccessToken.current() else {
//                print("Failed to get access token")
//                return
//            }
//            
//            let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)
//
//            // Perform login by calling Firebase APIs
//            Auth.auth().signInAndRetrieveData(with: credential, completion: { (user, error) in
//                if let error = error {
//                    print("Login error: \(error.localizedDescription)")
//                    let alertController = UIAlertController(title: "Login Error", message: error.localizedDescription, preferredStyle: .alert)
//                    let okayAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
//                    alertController.addAction(okayAction)
//                    self.present(alertController, animated: true, completion: nil)
//
//                    return
//                }
//
//                // Present the main view
//            Helper.helper.switchToNavigationViewController(Navigation: "homepage")
//
//            })
//
//        }
//
//    }
    
    
    
 
}



//extension UIView {
//
//    func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
//        let border = CALayer()
//        border.backgroundColor = color.cgColor
//        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: frame.size.width, height: width)
//        self.layer.addSublayer(border)
//        self.layer.masksToBounds = true
//    }
//
//    // OUTPUT 1
//    func dropShadow(scale: Bool = true) {
//        layer.masksToBounds = false
//        layer.shadowColor = UIColor.black.cgColor
//        layer.shadowOpacity = 0.5
//        layer.shadowOffset = CGSize(width: -1, height: 1)
//        layer.shadowRadius = 1
//
//        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
//        layer.shouldRasterize = true
//        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
//    }
//
//    // OUTPUT 2
//    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
//        layer.masksToBounds = false
//        layer.shadowColor = color.cgColor
//        layer.shadowOpacity = opacity
//        layer.shadowOffset = offSet
//        layer.shadowRadius = radius
//
//        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
//        layer.shouldRasterize = true
//        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
//    }
//
//}
extension UILabel {
    
    @IBInspectable
    var letterSpace: CGFloat {
        set {
            let attributedString: NSMutableAttributedString!
            if let currentAttrString = attributedText {
                attributedString = NSMutableAttributedString(attributedString: currentAttrString)
            }
            else {
                attributedString = NSMutableAttributedString(string: text ?? "")
                text = nil
            }
            
            attributedString.addAttribute(NSAttributedStringKey.kern,
                                          value: newValue,
                                          range: NSRange(location: 0, length: attributedString.length))
            
            attributedText = attributedString
        }
        
        get {
            if let currentLetterSpace = attributedText?.attribute(NSAttributedStringKey.kern, at: 0, effectiveRange: .none) as? CGFloat {
                return currentLetterSpace
            }
            else {
                return 0
            }
        }
    }
}



