//
//  BusinessProfileViewController.swift
//  iamu
//
//  Created by Muhammad Iqbal on 15/04/2018.
//  Copyright © 2018 Muhammad Iqbal. All rights reserved.
//

import UIKit

class BusinessProfileViewController: UIViewController {

    @IBOutlet weak var simpleSwitch: UISwitchExt!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = UIColor(red: 1, green: 0.56, blue: 0.27, alpha: 1)
               simpleSwitch.layer.cornerRadius = simpleSwitch.frame.height / 2
        Helper.helper.setupLeftNavItem(navigationItem: navigationItem)
        Helper.helper.setupRightNavItems(navigationItem: navigationItem)

        // Do any additional setup after loading the view.
    }
    
    @IBAction func switchProfile(_ sender: Any) {
        
        if (sender as AnyObject).isOn == false {
           Helper.helper.switchToNavigationViewController(Navigation: "myprofilepage")
        }
        
    }

}

