//
//  BusinessTableViewController.swift
//  iamu
//
//  Created by Muhammad Iqbal on 15/04/2018.
//  Copyright © 2018 Muhammad Iqbal. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import FirebaseFirestore

class BusinessTableViewController: UITableViewController {
    var industryCategory: [String] = []
    var typeOfBusiness: [String] = []
    var industrySize: [String] = []
    var employeeSize: [String] = []
    var month: [String] = []
    var year: [String] = []
    var city: [String] = []
    var state: [String] = []
    let days = ["Yes",
                "No"]
    
    @IBOutlet weak var linkedinTextField: UITextField!
    @IBOutlet weak var instagramTextField: UITextField!
    @IBOutlet weak var twitterTextField: UITextField!
    @IBOutlet weak var facebookTextField: UITextField!
    @IBOutlet weak var addressTextField2: UITextField!
    @IBOutlet weak var addressTextField1: UITextField!
    @IBOutlet weak var websiteUrlTextField: UITextField!
    @IBOutlet weak var incorporationDateTextField1: UITextField!
    
    @IBOutlet weak var incorporationDateTextField2: UITextField!
    @IBOutlet weak var employeeSizeTextField: UITextField!
    @IBOutlet weak var industrySizeTextField: UITextField!

    @IBOutlet weak var industryyCategoryPicker: UITextField!
    var selectedDay: String?
    @IBOutlet weak var registrationNumberTextField: UITextField!
    @IBOutlet weak var businessTypePicker: UITextField!
    let db = Firestore.firestore()
    var docRef : DocumentReference!
    let user = Auth.auth().currentUser

    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = UIColor(red: 1, green: 0.56, blue: 0.27, alpha: 1)
        getagensi()
        getindustri()
        getperniagaan()
        
        instagramTextField.font = UIFont(name: "Roboto-Regular", size: 12)
        twitterTextField.font = UIFont(name: "Roboto-Regular", size: 12)
        facebookTextField.font = UIFont(name: "Roboto-Regular", size: 12)
        addressTextField2.font = UIFont(name: "Roboto-Regular", size: 12)
        addressTextField1.font = UIFont(name: "Roboto-Regular", size: 12)
        websiteUrlTextField.font = UIFont(name: "Roboto-Regular", size: 12)
        incorporationDateTextField1.font = UIFont(name: "Roboto-Regular", size: 12)
        incorporationDateTextField2.font = UIFont(name: "Roboto-Regular", size: 12)
        employeeSizeTextField.font = UIFont(name: "Roboto-Regular", size: 12)
        industrySizeTextField.font = UIFont(name: "Roboto-Regular", size: 12)
        industryyCategoryPicker.font = UIFont(name: "Roboto-Regular", size: 12)
        registrationNumberTextField.font = UIFont(name: "Roboto-Regular", size: 12)
        businessTypePicker.font = UIFont(name: "Roboto-Regular", size: 12)
        
        city = ["PERLIS","KOTA SETAR","KUBANG PASU","PADANG TERAP","LANGKAWI","KUALA MUDA","YAN","SIK","BALING","KULIM","BANDAR BAHARU","PENDANG","POKOK SENA","BACHOK","KOTA BHARU","MACHANG","PASIR MAS","PASIR PUTEH","TANAH MERAH","TUMPAT","GUA MUSANG","KUALA KRAI","JELI","KECIL LOJING","BESUT","DUNGUN","DUN","KEMAMAN","KUALA TERENGGANU","HULU TERENGGANU","MARANG","SETIU","SET","SEBERANG PERAI TENGAH","SEBERANG PERAI UTARA","SEBERANG PERAI SELATAN","TIMOR LAUT","BARAT DAYA","BATANG PADANG","MANJUNG","KINTA","KERIAN","KUALA KANGSAR","LARUT & MATANG","HILIR PERAK","HULU PERAK","SELAMA","PERAK TENGAH","KAMPAR","MUALLIM","BENTONG","CAMERON HIGHLANDS","JERANTUT","KUANTAN","LIPIS","PEKAN","RAUB","TEMERLOH","ROMPIN","MARAN","BERA","KLANG","KUALA LANGAT","KUALA SELANGOR","SABAK BERNAM","ULU LANGAT","ULU SELANGOR","GOMBAK","SEPANG","W. P. KUALA LUMPUR","W. P. PUTRAJAYA","JELEBU","KUALA PILAH","PORT DICKSON","REMBAU","SEREMBAN","TAMPIN","JEMPOL","MELAKA","TENGAH","JASIN","ALOR GAJAH","BATU PAHAT","JOHOR BAHRU","KLUANG","KOTA TINGGI","MERSING","MUAR","PONTIAN","SEGAMAT","KULAIJAYA","LEDANG","W. P. LABUAN","KOTA KINABALU","PAPAR","KOTA BELUD","TUARAN","KUDAT","RANAU","SANDAKAN","LABUK & SUGUT","KINABATANGAN","TAWAU","LAHAD DATU","KENINGAU","TAMBUNAN","PENSIANGAN","TENOM","BEAUFORT","SEMPORNA","KUALA PENYU","SIPITANG","PENAMPANG","KOTA MARUDU","PITAS","KUNAK","TONGOD","PUTATAN","KUCHING","SRI AMAN","SIBU","MIRI","LIMBANG","SARIKEI","KAPIT","SAMARAHAN","BINTULU","MUKAH","BETONG"]
        
        city.sort { $0.compare($1, options: .numeric) == .orderedAscending }
        
        let date = Date()
        let calendar = Calendar.current

        
        let newyear =  calendar.component(.year, from: date)

        employeeSize = ["Self-employed", "1-10 employees", "11-50 employees" ,"51-200 employees", "201-500 employees", "501-1000 employees","1001-5000 employees" ,"5001-10,000 employees" ,"10,001+ employees"]
        month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
        let years = (1980...newyear).map { String($0) }
        year = years
        state = [
            "Johor",
            "Kedah",
            "Kelantan",
            "Melaka",
            "Negeri Sembilan",
            "Pahang",
            "Perak",
            "Perlis",
            "Pulau Pinang",
            "Sabah",
            "Sarawak",
            "Selangor",
            "Terengganu",
            "W.P. Kuala Lumpur",
            "W.P. Labuan",
            "W.P. Putrajaya"
        ]
        state.sort { $0.compare($1, options: .numeric) == .orderedAscending }
        
        //MARK Fetch data
        let emailUser = user!.email
        docRef = Firestore.firestore().document("User/\(emailUser ?? "")/Business Profile/\(emailUser ?? "")")
        getData()
        
        //MARK add image for dropdownbutton
        addImage(textfieldname: industryyCategoryPicker, imagename: "down")
        addImage(textfieldname: businessTypePicker, imagename: "down")
        addImage(textfieldname: industrySizeTextField, imagename: "down")
        addImage(textfieldname: employeeSizeTextField, imagename: "down")
        addImage(textfieldname: incorporationDateTextField1, imagename: "down")
        addImage(textfieldname: incorporationDateTextField2, imagename: "down")
        addImage(textfieldname: addressTextField1, imagename: "down")
        addImage(textfieldname: addressTextField2, imagename: "down")
        
        
       
        //MARK for UIPicker
        industryCategoryPicker(pickername: industryyCategoryPicker)
        createToolbar(pickername: industryyCategoryPicker)
        
        typeOfBusinessPicker(pickername: businessTypePicker)
        createToolbar(pickername: businessTypePicker)
        
        industrySizePicker(pickername: industrySizeTextField)
        createToolbar(pickername: industrySizeTextField)
        
        employeeSizePicker(pickername: employeeSizeTextField)
        createToolbar(pickername: employeeSizeTextField)
        
        monthPicker(pickername: incorporationDateTextField1)
        createToolbar(pickername: incorporationDateTextField1)
        
        yearPicker(pickername: incorporationDateTextField2)
        createToolbar(pickername: incorporationDateTextField2)
        
        cityPicker(pickername: addressTextField1)
        createToolbar(pickername: addressTextField1)
        
        statePicker(pickername: addressTextField2)
        createToolbar(pickername: addressTextField2)


    }
    
    func getData () {
        
        docRef.getDocument { (docSnapshot, error) in
            guard let docSnapshot = docSnapshot, docSnapshot.exists else { return }
            let data = docSnapshot.data()

            let address1 = data!["area"] as? String ?? ""
            let address2 = data!["country"] as? String ?? ""
            let fb = data!["facebook_Link"] as? String ?? ""
            let twitter = data!["twitter_Link"] as? String ?? ""
            let insta = data!["instagram_Link"] as? String ?? ""
            let linkedin = data!["linkedln"] as? String ?? ""
            let website = data!["website"] as? String ?? ""
            let date2 = data!["month"] as? String ?? ""
            let date1 = data!["year"] as? String ?? ""
            let employeeSize = data!["employee_Size"] as? String ?? ""
            let industrySize = data!["industry_Size"] as? String ?? ""
            let businessType = data!["business_Type"] as? String ?? ""
            let industryyCategory = data!["industry_Category"] as? String ?? ""
            let regNum = data!["company_Register"] as? String ?? ""
            
             self.addressTextField1.text! = address1
             self.addressTextField2.text! = address2
             self.facebookTextField.text! = fb
             self.twitterTextField.text! = twitter
             self.instagramTextField.text! = insta
             self.linkedinTextField.text! = linkedin
             self.websiteUrlTextField.text! = website
             self.incorporationDateTextField2.text! = date2
             self.incorporationDateTextField1.text! = date1
             self.employeeSizeTextField.text! = employeeSize
             self.industrySizeTextField.text! = industrySize
             self.businessTypePicker.text! = businessType
             self.industryyCategoryPicker.text! = industryyCategory
             self.registrationNumberTextField.text! = regNum
            
        }
        
    }
    
    func getagensi() {
        
        db.collection("AgencyType").getDocuments()
            {
                (querySnapshot, err) in
                
                if let err = err
                {
                    print("Error getting documents: \(err)");
                }
                else
                {
                    for document in querySnapshot!.documents {
                        let data = document.data()
                        
                        let programm = data["type"] as? String ?? ""
                        //  let agencyId = document.documentID
                        // print(programm)
                        //let program = Program(title: programm)
                        
                        self.typeOfBusiness.append(programm)
                        
                        
                        //self.programList.append(program)
                        
                        //print("\(document.documentID) => \(document.data())");
                    }
                    // print("Count = \(agencyNumber)");
                }
        }
        
    }
    
    func getindustri() {
        
        db.collection("Industri").getDocuments()
            {
                (querySnapshot, err) in
                
                if let err = err
                {
                    print("Error getting documents: \(err)");
                }
                else
                {
                    for document in querySnapshot!.documents {
                        let data = document.data()
                        
                        let programm = data["title"] as? String ?? ""
                        self.industryCategory.append(programm)
                        
                    }

                }
        }
        
    }
    
    func getperniagaan() {
        
        db.collection("Perniagaan").getDocuments()
            {
                (querySnapshot, err) in
                
                if let err = err
                {
                    print("Error getting documents: \(err)");
                }
                else
                {
                    for document in querySnapshot!.documents {
                        let data = document.data()
                        
                        let programm = data["title"] as? String ?? ""

                        
                          self.industrySize.append(programm)
                    }
                  
                    
                }
        }
        
    }
    
    @IBAction func saveBussinessProfileDidTapped(_ sender: Any) {
        
        let address1 = addressTextField1.text!
        let address2 = addressTextField2.text!
        let fb = facebookTextField.text!
        let twitter = twitterTextField.text!
        let insta = instagramTextField.text!
        let linkedin = linkedinTextField.text!
        let website = websiteUrlTextField.text!
        let date2 = incorporationDateTextField2.text!
        let date1 = incorporationDateTextField1.text!
        let employeeSize = employeeSizeTextField.text!
        let industrySize = industrySizeTextField.text!
        let businessType = businessTypePicker.text!
        let industryyCategory = industryyCategoryPicker.text!
        let regNum = registrationNumberTextField.text!
        
        let emailUser = user!.email
        docRef = Firestore.firestore().document("User/\(emailUser ?? "")/Business Profile/\(emailUser ?? "")")
        
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                
                self.docRef.updateData([
                    "company_Register": regNum,
                    "business_Type": businessType,
                    "industry_Category": industryyCategory,
                    "industry_Size": industrySize,
                    "employee_Size": employeeSize,
                    "month": date1,
                    "year": date2,
                    "website": website,
                    "area": address1,
                    "country": address2,
                    "facebook_Link": fb,
                    "twitter_Link": twitter,
                    "instagram_Link": insta,
                    "linkedln": linkedin
                ]) { err in
                    if let err = err {
                        print("Error updating document: \(err)")
                    } else {
                        Helper.showAlert(self, title: "Successful", message: "Your Business profile has been successfully updated")
                        print("Document successfully updated")
                    }
                }
                
            } else {
                self.docRef.setData([
                    "company_Register": regNum,
                    "business_Type": businessType,
                    "industry_Category": industryyCategory,
                    "industry_Size": industrySize,
                    "employee_Size": employeeSize,
                    "month": date1,
                    "year": date2,
                    "website": website,
                    "area": address1,
                    "country": address2,
                    "facebook_Link": fb,
                    "twitter_Link": twitter,
                    "instagram_Link": insta,
                    "linkedln": linkedin
                ]) { err in
                    if let err = err {
                        print("Error updating document: \(err)")
                    } else {
                        Helper.showAlert(self, title: "Successful", message: "Your Business profile has been successfully updated")
                    }
                }
                print("Document does not exist")
            }
        }

        
    }
    
    func addImage (textfieldname: UITextField, imagename: String) {
 
        textfieldname.rightViewMode = .always
       textfieldname.rightView = UIImageView(image: UIImage(named: imagename))

    }


}

extension BusinessTableViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    func industryCategoryPicker(pickername: UITextField) {
        
        let dayPicker = UIPickerView()
        dayPicker.tag = 1
        dayPicker.delegate = self
        
        pickername.inputView = dayPicker
        
        //Customizations
        dayPicker.backgroundColor = .white
    }
    
    func industrySizePicker(pickername: UITextField) {
        
        let dayPicker = UIPickerView()
        dayPicker.tag = 3
        dayPicker.delegate = self
        
        pickername.inputView = dayPicker
        
        //Customizations
        dayPicker.backgroundColor = .white
    }
    func employeeSizePicker(pickername: UITextField) {
        
        let dayPicker = UIPickerView()
        dayPicker.tag = 4
        dayPicker.delegate = self
        
        pickername.inputView = dayPicker
        
        //Customizations
        dayPicker.backgroundColor = .white
    }
    func typeOfBusinessPicker(pickername: UITextField) {
        
        let dayPicker = UIPickerView()
        dayPicker.tag = 2
        dayPicker.delegate = self
        
        pickername.inputView = dayPicker
        
        //Customizations
        dayPicker.backgroundColor = .white
    }
    func monthPicker(pickername: UITextField) {
        
        let dayPicker = UIPickerView()
        dayPicker.tag = 5
        dayPicker.delegate = self
        
        pickername.inputView = dayPicker
        
        //Customizations
        dayPicker.backgroundColor = .white
    }
    func yearPicker(pickername: UITextField) {
        
        let dayPicker = UIPickerView()
        dayPicker.tag = 6
        dayPicker.delegate = self
        
        pickername.inputView = dayPicker
        
        //Customizations
        dayPicker.backgroundColor = .white
    }
    func cityPicker(pickername: UITextField) {
        
        let dayPicker = UIPickerView()
        dayPicker.tag = 7
        dayPicker.delegate = self
        
        pickername.inputView = dayPicker
        
        //Customizations
        dayPicker.backgroundColor = .white
    }
    func statePicker(pickername: UITextField) {
        
        let dayPicker = UIPickerView()
        dayPicker.tag = 8
        dayPicker.delegate = self
        
        pickername.inputView = dayPicker
        
        //Customizations
        dayPicker.backgroundColor = .white
    }
    
    func createToolbar(pickername: UITextField) {
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        //Customizations
        toolBar.barTintColor = .orange
        toolBar.tintColor = .white
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(BusinessTableViewController.dismissKeyboard))
        
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        pickername.inputAccessoryView = toolBar
    }
    
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch pickerView.tag {
        case 1:
            return industryCategory.count
        case 2:
            return typeOfBusiness.count
        case 3:
            return industrySize.count
        case 4:
            return employeeSize.count
        case 5:
            return month.count
        case 6:
            return year.count
        case 7:
            return city.count
        case 8:
            return state.count
        default:
            return days.count
        }
        
//        if (pickerView.tag == 1){
//            return days.count
//        }else{
//            return picker1Options.count
//        }
        
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        switch pickerView.tag {
        case 1:
            return industryCategory[row].capitalized
        case 2:
            return typeOfBusiness[row].capitalized
        case 3:
            return industrySize[row].capitalized
        case 4:
            return employeeSize[row].capitalized
        case 5:
            return month[row].capitalized
        case 6:
            return year[row].capitalized
        case 7:
            return city[row].capitalized
        case 8:
            return state[row].capitalized
        default:
            return days[row].capitalized
        }
        
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
        case 1:
            selectedDay = industryCategory[row].capitalized
            industryyCategoryPicker.text = selectedDay
        case 2:
            selectedDay = typeOfBusiness[row].capitalized
            businessTypePicker.text = selectedDay
        case 3:
            selectedDay = industrySize[row].capitalized
            industrySizeTextField.text = selectedDay
        case 4:
            selectedDay = employeeSize[row].capitalized
            employeeSizeTextField.text = selectedDay
        case 5:
            selectedDay = month[row].capitalized
            incorporationDateTextField1.text = selectedDay
        case 6:
            selectedDay = year[row].capitalized
            incorporationDateTextField2.text = selectedDay
        case 7:
            selectedDay = city[row].capitalized
            addressTextField1.text = selectedDay
        case 8:
            selectedDay = state[row].capitalized
            addressTextField2.text = selectedDay
        default:
            selectedDay = days[row].capitalized
            addressTextField2.text = selectedDay
        }
        

    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var label: UILabel
        
        if let view = view as? UILabel {
            label = view
        } else {
            label = UILabel()
        }
        
        label.textColor = .orange
        label.textAlignment = .center
        label.font = UIFont(name: "Montserrat", size: 30)
        
        switch pickerView.tag {
        case 1:
            label.text = industryCategory[row].capitalized
        case 2:
            label.text = typeOfBusiness[row].capitalized
        case 3:
            label.text = industrySize[row].capitalized
        case 4:
            label.text = employeeSize[row].capitalized
        case 5:
            label.text = month[row].capitalized
        case 6:
            label.text = year[row].capitalized
        case 7:
            label.text = city[row].capitalized
        case 8:
            label.text = state[row].capitalized
        default:
            label.text = days[row].capitalized
        }        
        
        
        return label
    }
}



