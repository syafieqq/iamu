//
//  Helper.swift
//  iamu
//
//  Created by Muhammad Iqbal on 15/04/2018.
//  Copyright © 2018 Muhammad Iqbal. All rights reserved.
//

import Foundation
import UIKit
class Helper {
    static let helper = Helper()
    
    static func showAlert(_ inViewController: UIViewController, title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        inViewController.present(alert, animated: true, completion: nil)
    }
    
    func test(vc: UIViewController) {
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        alert.view.tintColor = UIColor(red: 1, green: 0.56, blue: 0.27, alpha: 1)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        //loadingIndicator.backgroundColor = UIColor(red: 1, green: 0.56, blue: 0.27, alpha: 1)
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
         vc.present(alert, animated: true, completion: nil)
    }

    func switchToNavigationViewController(Navigation : String ) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let naviVC = storyboard.instantiateViewController(withIdentifier: Navigation) as! UINavigationController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = naviVC
        
    }
    
    func switchToViewController(Navigation : String ) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let naviVC = storyboard.instantiateViewController(withIdentifier: Navigation)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = naviVC
        
    }
    
    func iconTextField (textField : UITextField, imageName : String) {

        let leftImageView = UIImageView()
        leftImageView.image = UIImage(named: imageName)
        
        let leftView = UIView()
        leftView.addSubview(leftImageView)
        
        leftView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        leftImageView.frame = CGRect(x: 10, y: 10, width: 30, height: 30)
        textField.leftViewMode = .always
        textField.leftView = leftView
        
        //add border bottom
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.gray.cgColor
        border.frame = CGRect(x: 0, y: textField.frame.size.height - width, width:  textField.frame.size.width, height: textField.frame.size.height)
        
        border.borderWidth = width
        textField.layer.addSublayer(border)
        textField.layer.masksToBounds = true
        
    }
    
    func shadow (myView: UIView) {
        myView.layer.shadowColor = UIColor.black.cgColor
        myView.layer.shadowOpacity = 0.25
        myView.layer.shadowRadius = 3
        myView.layer.shadowOffset = CGSize(width: 2, height: 2)
    }
    
    func lineTextField (textField : UITextField) {
        
        //add border bottom
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.gray.cgColor
        border.frame = CGRect(x: 0, y: textField.frame.size.height - width, width:  textField.frame.size.width, height: textField.frame.size.height)
        
        border.borderWidth = width
        textField.layer.addSublayer(border)
        textField.layer.masksToBounds = true
        
    }
    
    func setupLeftNavItem(navigationItem: UINavigationItem) {
        let followButton = UIButton(type: .custom)
        followButton.setImage(#imageLiteral(resourceName: "menu_white").withRenderingMode(.alwaysTemplate), for: .normal)
        followButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        followButton.addTarget(self, action: #selector(menuTapped), for: .touchUpInside)
        followButton.tintColor = UIColor(white: 1, alpha: 1)
        
     //   navigationItem.leftBarButtonItem = UIBarButtonItem(customView: followButton)
        

            let menuBarItem = UIBarButtonItem(customView: followButton)
            let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 30)
            currWidth?.isActive = true
            let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 30)
            currHeight?.isActive = true
            navigationItem.leftBarButtonItem = menuBarItem

        
        
    }
    
    @objc func menuTapped() {
        switchToNavigationViewController(Navigation: "homepage")
        print("menu did tapped")
        
    }
    
    func setupRightNavItems(navigationItem: UINavigationItem) {
//        let filterButton = UIButton(type: .system)
//        filterButton.setImage(#imageLiteral(resourceName: "newfilter1").withRenderingMode(.alwaysOriginal), for: .normal)
//        filterButton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
//        filterButton.tintColor = UIColor.groupTableViewBackground
//
//        filterButton.addTarget(self, action: #selector(filterTapped), for: .touchUpInside)
        
        
        let profileButton = UIButton(type: .system)
        profileButton.setImage(#imageLiteral(resourceName: "newmale1-1").withRenderingMode(.alwaysTemplate), for: .normal)
        profileButton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
          profileButton.tintColor = UIColor(white: 1, alpha: 0.3)
        profileButton.addTarget(self, action: #selector(profileTapped), for: .touchUpInside)

            
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: profileButton)
    }
    
    @objc func filterTapped() {

        print("filter did tapped")


    }
    
    @objc func profileTapped() {
        Helper.helper.switchToNavigationViewController(Navigation: "myprofilepage")
        print("profile did tapped")
        
    }

    
}


