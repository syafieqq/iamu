//
//  AgencyTableViewController.swift
//  iamu
//
//  Created by Muhammad Iqbal on 16/04/2018.
//  Copyright © 2018 Muhammad Iqbal. All rights reserved.
//

import UIKit
import TagListView
import Firebase
import FirebaseAuth
import FirebaseFirestore
import SDWebImage
import Material

struct DisplayModel2 {
    
    var agency1: String?
    var agencyId1: String?
    var programTag1: String?
    var perniagaanTag1: String?
    var max1: Int?
    var agencyPath1: String?
    var logoUrl1: String?
    var agencyTitle1: String?
    
}
class AgencyTableViewController: UITableViewController {
    var agencyPath = ""
    
    @IBOutlet var tableView2: UITableView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var nameTextField: UILabel!
    @IBOutlet weak var descriptionTextField: UILabel!
    @IBOutlet weak var addressTextField: UILabel!
    @IBOutlet weak var telTextField: UILabel!
    @IBOutlet weak var faxTextField: UILabel!
    @IBOutlet weak var emailTextField: UILabel!
    @IBOutlet weak var websiteTextField: UILabel!
    @IBOutlet weak var agencyNameTextField: UILabel!
    var programList1 = [DisplayModel2]()
    var boxView = UIView()
    let font = UIFont(name: "Roboto", size: 12.0)

    var heightOfAgency: CGFloat = 1
    var heightOfAddress2: CGFloat = 1
    var heightOfemail: CGFloat = 1
    var heightOftel: CGFloat = 1
    var heightOffax: CGFloat = 1
    var heightOfWebsite: CGFloat = 1
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        //        self.navigationController?.navigationBar.barTintColor = UIColor.white
        
        self.navigationItem.title = "Agency"
    }
    

    
    var loadingView : LoadingView!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = UIColor(red: 1, green: 0.56, blue: 0.27, alpha: 1)
        //test()
        Helper.helper.test(vc: self)
        print ("my new agency path is: \(agencyPath)")
        getDataProgram()
        getData()
sizeFooterToFit()
        self.tableView2.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        guard let headerView = tableView2.tableHeaderView else {
            return
        }
        
        let size = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        if headerView.frame.size.height != size.height {
            headerView.frame.size.height = size.height
            tableView2.tableHeaderView = headerView
            tableView2.layoutIfNeeded()
    }
        
    }
    
    @IBOutlet weak var footerTableView: UIView!
    
    func sizeFooterToFit() {
        let footerView = tableView.tableFooterView!
        
        footerView.setNeedsLayout()
        footerView.layoutIfNeeded()
        
 //       let height = footerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
        var frame = footerView.frame
        frame.size.height = (375 + heightOfAddress2 + heightOffax + heightOftel + heightOfemail + heightOfAgency + heightOfWebsite)
        footerView.frame = frame
        tableView.reloadData()
        tableView.tableFooterView = footerView
    }
    

    
//    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//               return (375 + heightOfAddress2 + heightOffax + heightOftel + heightOfemail + heightOfAgency + heightOfWebsite)
//    }
//
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return programList1.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellData: DisplayModel2
        cellData = programList1[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as? TableLoopTableViewCell
              
        let tapGesture = UITapGestureRecognizer (target: self, action: #selector(imgTap(tapGesture:)))
        tapGesture.numberOfTapsRequired = 1
        
        cell?.label.text = cellData.agency1
        cell?.companyImage.sd_setImage(with: URL(string: "\(cellData.logoUrl1 ?? "")"), placeholderImage: UIImage(named: "no_pic_image"))
        cell?.companyImage.addGestureRecognizer(tapGesture)
        cell?.companyImage.isUserInteractionEnabled = true
        cell?.companyImage.tag = indexPath.row
        cell?.tagListView.removeAllTags()
        cell?.tagListView.textFont = UIFont.systemFont(ofSize: 10)
        cell?.tagListView.alignment = .left
        cell?.tagListView.addTag(cellData.programTag1!)
        cell?.tagListView.addTag("Industri \(cellData.perniagaanTag1!)")
        
        self.dismiss(animated: false, completion: nil)
        return cell!
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didTapMessageAtIndexPath: \(indexPath.item)")
        print("didTapMessageAtIndexPath: \(indexPath.row)")
        
        let cellData: DisplayModel2
        cellData = programList1[indexPath.row]
        
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let desVC = mainStoryboard.instantiateViewController(withIdentifier: "detailprogrampage") as! ProgramTableViewController
        desVC.agencyId = cellData.agencyId1!
        self.navigationController?.pushViewController(desVC, animated: true)
        
        
    }

    
    @objc func imgTap(tapGesture: UITapGestureRecognizer) {
        let imgView = tapGesture.view as! UIImageView
        print(imgView.tag)
        
    }
    
    func getData () {
        
        let queryy = Firestore.firestore().collection("Agensi")
        let newQueryy = queryy.whereField("name", isEqualTo: "\(agencyPath)")
        newQueryy.getDocuments()
            {
                (querySnapshot, err) in
                
                if let err = err
                {
                    print("Error getting documents: \(err)");
                }
                else
                {
                    //  var agencyNumber = 0
                    for document in querySnapshot!.documents {
                        let data = document.data()
                        
                        let name = data["name"] as? String ?? ""
                        let logo = data["logo"] as? String ?? ""
                        let description = data["description"] as? String ?? ""
                        let address = data["address"] as? String ?? ""
                        let tel = data["tel"]
                        let fax = data["fax"]
                        let email = data["email"] as? String ?? ""
                        let website = data["website"] as? String ?? ""
                        self.logoImage.sd_setImage(with: URL(string: "\(logo)"), placeholderImage: UIImage(named: "no_pic_image"))
                        
                        print ("my tel number is : \(tel ?? "-")")
                        print ("my fax number is : \(tel ?? "-")")
                        
                        self.nameTextField.text! = name
                        self.agencyNameTextField.text! = name
                        self.descriptionTextField.text! = description
                        self.addressTextField.text! = address
                        self.telTextField.text! = "\(tel ?? "-")"
                        self.faxTextField.text! = "\(fax ?? "-")"
                        self.emailTextField.text! = email
                        self.websiteTextField.text! = website
                        
                        let heighth = self.agencyNameTextField.text?.heightWithConstrainedWidth(width: UIScreen.main.bounds.width - 64 , font: self.font!)
                        let heighth2 = self.emailTextField.text?.heightWithConstrainedWidth(width: UIScreen.main.bounds.width - 64 , font: self.font!)
                        let heighth3 = self.addressTextField.text?.heightWithConstrainedWidth(width: UIScreen.main.bounds.width - 64 , font: self.font!)
                        let heighth4 = self.telTextField.text?.heightWithConstrainedWidth(width: UIScreen.main.bounds.width - 64 , font: self.font!)
                        let heighth5 = self.faxTextField.text?.heightWithConstrainedWidth(width: UIScreen.main.bounds.width - 64 , font: self.font!)
                        let heighth6 = self.websiteTextField.text?.heightWithConstrainedWidth(width: UIScreen.main.bounds.width - 64 , font: self.font!)
                        
                        
                        self.heightOfAgency = heighth ?? 10
                        self.heightOfAddress2 = heighth3 ?? 10
                        self.heightOfemail = heighth2 ?? 10
                        self.heightOftel = heighth4 ?? 10
                        self.heightOffax = heighth5 ?? 10
                        self.heightOfWebsite = heighth6 ?? 10
                        
                        self.footerTableView.frame.size.height = (325 + self.heightOfAddress2 + self.heightOffax + self.heightOftel + self.heightOfemail + self.heightOfAgency + self.heightOfWebsite)
                        
                        self.tableView2.reloadData()
                        
                       
                        
                    }
                }
        }
        
    }
    
    func getDataProgram()  {

        Firestore.firestore().collection("fund").whereField("agensi", isEqualTo: agencyPath).getDocuments()
            {
                (querySnapshot, err) in
                
                if let err = err
                {
                    print("Error getting documents: \(err)");
                }
                else
                {
                    //  var agencyNumber = 0
                    for document in querySnapshot!.documents {
                        let data = document.data()
                        let agencyPath = data["agensi"] as? String ?? ""
                        let title = data["title"] as? String ?? ""
                        let program = data["program"] as? String ?? ""
                        let perniagaan = data["perniagaan"] as? String ?? ""
                        let newMax = data["max"] as? Int
                        let agencyId = document.documentID
                        print(title)
                        print(agencyPath)
                        print(agencyId)
                        
                        let queryy = Firestore.firestore().collection("Agensi")
                        let newQueryy = queryy.whereField("name", isEqualTo: "\(agencyPath)")
                        newQueryy.getDocuments()
                            {
                                (querySnapshot, err) in
                                
                                if let err = err
                                {
                                    print("Error getting documents: \(err)");
                                }
                                else
                                {
                                    //  var agencyNumber = 0
                                    for document in querySnapshot!.documents {
                                        let data = document.data()
                                        
                                        let logo = data["logo"] as? String ?? ""
                                        
                                        let newModel = DisplayModel2(agency1: title, agencyId1: agencyId, programTag1: program, perniagaanTag1: perniagaan, max1: newMax, agencyPath1: agencyPath, logoUrl1: logo, agencyTitle1: agencyPath)
                                        self.programList1.append(newModel)
  
                                    }
                                     self.tableView2.reloadData()
                                     self.dismiss(animated: false, completion: nil)
                                }
                        }
                        
                        
                    }
                    
                    
                }
        }
    }
    
    

}

