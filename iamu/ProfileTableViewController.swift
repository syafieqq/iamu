//
//  ProfileTableViewController.swift
//  iamu
//
//  Created by Muhammad Iqbal on 16/04/2018.
//  Copyright © 2018 Muhammad Iqbal. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import FirebaseFirestore

class ProfileTableViewController: UITableViewController {

        @IBOutlet weak var nameTextField: UITextField!
        @IBOutlet weak var emailTextField: UITextField!
        @IBOutlet weak var mobileTextField: UITextField!
    
    @IBOutlet weak var popOutPage: UIView!
    
    let db = Firestore.firestore()
    var docRef : DocumentReference!
    let user = Auth.auth().currentUser
    
    
    @IBAction func okayButtonInMIdDidTapped(_ sender: Any) {
        popOutPage.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       popOutPage.isHidden = true
        navigationController?.navigationBar.barTintColor = UIColor(red: 1, green: 0.56, blue: 0.27, alpha: 1)
        nameTextField.font = UIFont(name: "Roboto-Regular", size: 12)
        emailTextField.font = UIFont(name: "Roboto-Regular", size: 12)
        mobileTextField.font = UIFont(name: "Roboto-Regular", size: 12)
        
        let emailUser = user!.email
        docRef = Firestore.firestore().document("User/\(emailUser ?? "")")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getData()
    }
        @IBAction func connectInstaButton(_ sender: UIButton) {
            
            let button = sender
            
            if button.isSelected == true {
                
                button.backgroundColor = UIColor(red:0.98, green:0.98, blue:0.98, alpha:1.0)
                button.setTitle("Connect Instagram",for: .normal)//Choose your
                button.isSelected = false
                
            } else {
                button.backgroundColor = UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)
                button.setTitleColor(UIColor(red:0.51, green:0.51, blue:0.51, alpha:1.0), for: .normal)
                button.setTitle("Disconnect Instagram",for: .normal)
                button.isSelected = true
                
                
            }
            
        }
        
        @IBAction func connectFbButton(_ sender: UIButton) {
            
            let button = sender
            
            if button.isSelected == true {
                
                button.backgroundColor = UIColor(red:0.98, green:0.98, blue:0.98, alpha:1.0)
                button.setTitle("Connect Facebook",for: .normal)
                button.isSelected = false
                
            } else {
                button.backgroundColor = UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)
                button.setTitleColor(UIColor(red:0.51, green:0.51, blue:0.51, alpha:1.0), for: .normal)
                button.setTitle("Disconnect Facebook",for: .normal)
                button.isSelected = true
                
                
            }
            
        }

    @IBAction func saveButtonDidTapped(_ sender: Any) {
    
        guard
            let name = nameTextField.text,
            name != "",
            let mobile = mobileTextField.text,
            mobile != "",
            let email = emailTextField.text,
            email != ""

            else {
                Helper.showAlert(self, title: "Missing Info", message: "Please fill out all fields")
                return
        }
        
        let emailUser = user!.email
        docRef = Firestore.firestore().document("User/\(emailUser ?? "")")
        
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
            
                self.db.collection("User").document("\(emailUser ?? "")").updateData([
                    "email": email,
                    "full_Name": name,
                    "mobile_number": mobile
                ]) { err in
                    if let err = err {
                        print("Error updating document: \(err)")
                    } else {
                         Helper.showAlert(self, title: "Successful", message: "Your profile has been successfully updated")
                        print("Document successfully updated")
                    }
                }
                
            } else {
                self.db.collection("User").document("\(emailUser ?? "")").setData([
                    "email": email,
                    "full_Name": name,
                    "mobile_number": mobile
                ]) { err in
                    if let err = err {
                        print("Error updating document: \(err)")
                    } else {
                        Helper.showAlert(self, title: "Successful", message: "You have succesfully update your profile")
                        print("Document successfully updated")
                    }
                }
                print("Document does not exist")
            }
        }

        
    }
    
    @IBAction func logoutButtonDidTapped(_ sender: Any) {
        
        do {
            try Auth.auth().signOut()
            
            Helper.helper.switchToViewController(Navigation: "loginpage")
            
        } catch {
            print(error)
        }
        
    }
    
    func getData () {
        
        docRef.getDocument { (docSnapshot, error) in
            guard let docSnapshot = docSnapshot, docSnapshot.exists else { return }
            let data = docSnapshot.data()
            let email = data!["email"] as? String ?? ""
            let fullName = data!["full_Name"] as? String ?? ""
            let mobileNumber = data!["mobile_number"] as? String ?? ""

            self.emailTextField.text! = "\(email)"
            self.nameTextField.text! = fullName
            self.mobileTextField.text! = mobileNumber

        }

    }
}
