//
//  HomeViewController.swift
//  iamu
//
//  Created by Muhammad Iqbal on 18/04/2018.
//  Copyright © 2018 Muhammad Iqbal. All rights reserved.
//

import UIKit
import WARangeSlider
import TagListView
import Firebase
import FirebaseAuth
import FirebaseFirestore
import SDWebImage


struct DisplayModel {
    
    var agency: String?
    var agencyId: String?
    var programTag: String?
    var perniagaanTag: String?
    var max: Int?
    var agencyPath: String?
    var logoUrl: String?
    var agencyTitle: String?
    
}

struct DisplayModel3 {
    
    var agency: String?
    var agencyId: String?
    var programTag: String?
    var perniagaanTag: String?
    var max: Int?
    var agencyPath: String?
    var logoUrl: String?
    var agencyTitle: String?
    
}

class HomeViewController: UIViewController {
 
    @IBOutlet weak var buttonView: CardView!
    @IBOutlet weak var detailsView: UIView!
    @IBOutlet weak var multiplierCons: NSLayoutConstraint!
    @IBOutlet weak var tagView: TagListView!
    @IBOutlet weak var maxLabel: UILabel!
    @IBOutlet weak var minLabel: UILabel!
    @IBOutlet weak var maxLabel2: UILabel!
    @IBOutlet weak var minLabel2: UILabel!
    @IBOutlet weak var rangeSlider1: RangeSlider!
    @IBOutlet weak var rangeSlider2: RangeSlider!
    @IBOutlet weak var programCategoryDropDown: UITextField!
    @IBOutlet weak var agencyCategoryDropDown: UITextField!
    @IBOutlet weak var industryCategoryDropDown: UITextField!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var headerView: UIView!
    @IBOutlet var footerView: UIView!
    @IBOutlet weak var searchResultLabel: UILabel!
    

    let db = Firestore.firestore()
    var docRef : DocumentReference!
    let user = Auth.auth().currentUser
    var sideMenuOpen = false
    
    //MARK var save array of data
    var agencyList = [DisplayModel]()
    //var programStorage = [DisplayModel4]()
   // var originalData: [String] = []
    var filteredData = [DisplayModel]()
    var agency: [String] = []
    var industry: [String] = []
    var program: [String] = []
    var perniagaan: [String] = []
    
    //MARK constant and var for searchcontroller
    var searchController = UISearchController(searchResultsController: nil)
    var newSearchString = ""
    
    //MARK var for filter
    var filterIndustri = ""
    var filterAgencyType = ""
    var filterProgram = ""
    var filterPerniagaan = ""
    var filterMin : Int?
    var filterMax = 1500000
    var selectedDay: String?
    let step: Int = 1000
    let pagingSpinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)

    var lastData: QueryDocumentSnapshot!
    var lastFilterData: QueryDocumentSnapshot!
    
    
    override func viewDidLoad() {
    super.viewDidLoad()

     UserDefaults.standard.set(nil, forKey: "filter")

        self.alertPopOut.isHidden = true
        let emailUser = user!.email
        docRef = Firestore.firestore().document("User/\(emailUser ?? "")")
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 1, green: 0.56, blue: 0.27, alpha: 1)
        

        if 30 < agencyList.count {
        //    self.tableView.reloadData()
            Helper.helper.test(vc: self)

        } else {
            if tableView != nil {
                Helper.helper.test(vc: self)
                getData()
                // keepData()
                self.tableView.tableHeaderView = nil;
                self.tableView.tableFooterView = nil;
            }

        }
        

        
        
        searchController = UISearchController(searchResultsController: nil)
        //MARK for searchBar UI
        self.searchController.dimsBackgroundDuringPresentation = false
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.searchBar.placeholder = " Search for programs or agencies..."
        self.searchController.searchBar.sizeToFit()
        self.searchController.searchBar.isTranslucent = false
        self.searchController.searchBar.backgroundImage = UIImage()
        self.searchController.searchBar.delegate = self
        self.searchController.searchBar.clipsToBounds = true

        self.navigationItem.titleView = searchController.searchBar
        
       //  SearchBar text
        let cancelButtonAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
        let textFieldInsideUISearchBar = searchController.searchBar.value(forKey: "searchField") as? UITextField
        
        textFieldInsideUISearchBar?.borderStyle = .roundedRect
        textFieldInsideUISearchBar?.font = UIFont(name: "Roboto-Regular", size: 12)
        if let backgroundview = textFieldInsideUISearchBar?.subviews.first {
                backgroundview.layer.cornerRadius = 20;
                backgroundview.clipsToBounds = true;
            }
        

        
        if programCategoryDropDown != nil {
            //MARK print uipicker list
            getprogram()
            getagensi()
            getindustri()

            let myColor : UIColor =  UIColor(white: 1, alpha: 0.3)
            programCategoryDropDown.layer.borderWidth = 2
            programCategoryDropDown.layer.borderColor = myColor.cgColor
            programCategoryDropDown.layer.cornerRadius = programCategoryDropDown.frame.height/2
            agencyCategoryDropDown.layer.borderWidth = 2
            agencyCategoryDropDown.layer.borderColor = myColor.cgColor
            agencyCategoryDropDown.layer.cornerRadius = agencyCategoryDropDown.frame.height/2
            industryCategoryDropDown.layer.borderWidth = 2
            industryCategoryDropDown.layer.borderColor = myColor.cgColor
            industryCategoryDropDown.layer.cornerRadius = industryCategoryDropDown.frame.height/2
            
            //MARK tag view
            getperniagaan()
           // print ("new pernigaan is : \(program)")
            
            
            //MARK filter slider
            rangeSlider1.addTarget(self, action: #selector(HomeViewController.rangeSliderValueChanged(_:)), for: .valueChanged)
            
            rangeSlider2.addTarget(self, action: #selector(HomeViewController.rangeSliderValueChanged2(_:)), for: .valueChanged)
            
            
            //MARK Filter toggle
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(toggleSideMenu),
                                                   name: NSNotification.Name("ToggleSideMenu"),
                                                   object: nil)
            
            //MARK Navigation bar icon
           // Helper.helper.setupLeftNavItem(navigationItem: navigationItem)
            setupRightNavItems(navigationItem: navigationItem)
            setupLeftNavItem(navigationItem: navigationItem)
            
            
            //MARK for UIPicker
            agencyPicker(pickername: agencyCategoryDropDown)
            createToolbar(pickername: agencyCategoryDropDown)
            industryPicker(pickername: industryCategoryDropDown)
            createToolbar(pickername: industryCategoryDropDown)
            programPicker(pickername: programCategoryDropDown)
            createToolbar(pickername: programCategoryDropDown)
            
            //MARK add aditional tag
        }
    }
    
    override func viewDidLayoutSubviews() {
        
        view.bringSubview(toFront: alertPopOut)
        
    }
    

    func getData()  {
        self.agencyList.removeAll()
        let first =
            db.collection("fund")
            //    .limit(to: 20)

        first.addSnapshotListener { (snapshot, error) in
            guard let snapshot = snapshot else {
                print("Error retreving cities: \(error.debugDescription)")
                return
            }

            //snapshot.getDocuments()
            for document in snapshot.documents {

                //for document in querySnapshot!.documents {
                let data = document.data()
                let agencyPath = data["agensi"] as? String ?? ""
                let title = data["title"] as? String ?? ""
                let program = data["program"] as? String ?? ""
                let perniagaan = data["perniagaan"] as? String ?? ""
                let newMax = data["max"] as? Int
                let agencyId = document.documentID

                let query = Firestore.firestore().collection("Agensi")
                let newQuery = query.whereField("name", isEqualTo: "\(agencyPath)")
                var logo = ""

                newQuery.getDocuments()
                    {
                        (querySnapshot, err) in

                        if let err = err
                        {
                            print("Error getting documents: \(err)");
                        }
                        else
                        {
                            //  var agencyNumber = 0
                            for document in querySnapshot!.documents {
                                let data = document.data()
                                logo = data["logo"] as? String ?? ""
                                //start
                        }
                      //  print("here4")
                }
                        let newModel = DisplayModel(agency: title, agencyId: agencyId, programTag: program, perniagaanTag: perniagaan, max: newMax, agencyPath: agencyPath, logoUrl: logo, agencyTitle: agencyPath)
                        self.agencyList.append(newModel)
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                }
                

              //  print ("agency list array is : \(self.agencyList)")
            }
          //  self.lastData = snapshot.documents.last
        }
    }

    
    
    @objc func filterTapped() {
       
        
        if sideMenuOpen == true {
            self.agencyList.removeAll()
             NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
            UserDefaults.standard.set("filter", forKey: "filter")
            Helper.helper.test(vc: self)
           // UserDefaults.standard.set("", forKey: "key")
            var newQuery: Query = self.query
                //.limit(to: 20)


            
            if self.filterPerniagaan != "" {
                newQuery = newQuery.whereField("perniagaan", isEqualTo: self.filterPerniagaan.capitalized )
            }
            if self.filterIndustri != "" {
                newQuery = newQuery.whereField("industri", isEqualTo: self.filterIndustri.capitalized )
            }
            if self.filterAgencyType != "" {
                newQuery =  newQuery.whereField("type", isEqualTo: self.filterAgencyType.capitalized)
            }
            if self.filterProgram != "" {
                newQuery = newQuery.whereField("program", isEqualTo: self.filterProgram.capitalized )
            }
            if self.filterMin != nil {
                newQuery = newQuery.whereField("min", isGreaterThanOrEqualTo : self.filterMin as Any)
            }
            
            newQuery.addSnapshotListener { (snapshot, error) in
                    
                    guard let snapshot = snapshot else {
                        print("Error retreving cities: \(error.debugDescription)")
                        return
                    }

                for document in snapshot.documents {
                        //  var agencyNumber = 0
                    
                            let data = document.data()
                            let agencyPath = data["agensi"] as? String ?? ""
                            let title = data["title"] as? String ?? ""
                            let program = data["program"] as? String ?? ""
                            let perniagaan = data["perniagaan"] as? String ?? ""
                            let newMax = data["max"] as? Int
                            let agencyId = document.documentID
                            
                            //start
                            let query = Firestore.firestore().collection("Agensi")
                            let newQuery = query.whereField("name", isEqualTo: "\(agencyPath)")
                            
                            newQuery.getDocuments()
                                {
                                    (querySnapshot, err) in
                                    
                                    if let err = err
                                    {
                                        print("Error getting documents: \(err)");
                                    }
                                    else
                                    {
                                        //  var agencyNumber = 0
                                        for document in querySnapshot!.documents {
                                            let data = document.data()
                                            let logo = data["logo"] as? String ?? ""
                                            
                                            let newModel = DisplayModel(agency: title, agencyId: agencyId, programTag: program, perniagaanTag: perniagaan, max: newMax, agencyPath: agencyPath, logoUrl: logo, agencyTitle: agencyPath)
                                            
//                                            let newModelArray = [newModel]
                                            self.agencyList.append(newModel)
                                            
                                            if self.filterMax != 1500000 {
                                                self.agencyList = self.agencyList.filter { $0.max! <= self.filterMax }
                                                
//                                                let filteredArray = newModelArray.filter { $0.max! <= self.filterMax }
//                                                self.agencyList = filteredArray
//                                                print("my filtermax is \(self.agencyList)")
//                                               // self.tableView.reloadData()
                                                
                                            } else {
                                                
                                              //  self.tableView.reloadData()
//                                                print ("current agency list is \(self.agencyList)")
                                            }

                                            
//                                            print ("perniagaan is \(self.filterPerniagaan)")
//                                            print ("perniagaan is \(self.filterAgencyType)")
//                                            print ("perniagaan is \(self.filterProgram)")
//                                            print ("perniagaan is \(self.filterIndustri)")
                                            
                                        }
                                        DispatchQueue.main.async {
                                            self.tableView.reloadData()
                                        }
  //                                      self.lastFilterData = snapshot.documents.last
                                        
                                    }
                            }
                        }
                
                        self.tableView.reloadData()
                    
            }
            self.dismiss(animated: false, completion: nil)
            
        } else {
             NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
        }

    }
    
    
    func getprogram() {

        db.collection("Program").getDocuments()
            {
                (querySnapshot, err) in

                if let err = err
                {
                    print("Error getting documents: \(err)");
                }
                else
                {
                    for document in querySnapshot!.documents {
                        let data = document.data()

                        let programm = data["title"] as? String ?? ""
 //                       let programId = document.documentID
 //                       print("program id is: \(programId)")

                        self.program.append(programm)

                    }
                    // print("Count = \(agencyNumber)");
                }
        }

    }
    
    func getagensi() {
        
        db.collection("AgencyType").getDocuments()
            {
                (querySnapshot, err) in
                
                if let err = err
                {
                    print("Error getting documents: \(err)");
                }
                else
                {
                    for document in querySnapshot!.documents {
                        let data = document.data()
                        
                        let programm = data["type"] as? String ?? ""
                        //  let agencyId = document.documentID
                       // print(programm)
                        //let program = Program(title: programm)
                        
                        self.agency.append(programm)
                        
                        
                        //self.programList.append(program)
                        
                        //print("\(document.documentID) => \(document.data())");
                    }
                    // print("Count = \(agencyNumber)");
                }
        }
        
    }
    
    func getindustri() {
        
        db.collection("Industri").getDocuments()
            {
                (querySnapshot, err) in
                
                if let err = err
                {
                    print("Error getting documents: \(err)");
                }
                else
                {
                    for document in querySnapshot!.documents {
                        let data = document.data()
                        
                        let programm = data["title"] as? String ?? ""
                        //  let agencyId = document.documentID
                      //  print(programm)
                        //let program = Program(title: programm)
                        
                        self.industry.append(programm)
                        
                        
                        //self.programList.append(program)
                        
                        //print("\(document.documentID) => \(document.data())");
                    }
                    // print("Count = \(agencyNumber)");
                }
        }
        
    }
    
    func getperniagaan() {
        
        db.collection("Perniagaan").getDocuments()
            {
                (querySnapshot, err) in
                
                if let err = err
                {
                    print("Error getting documents: \(err)");
                }
                else
                {
                    for document in querySnapshot!.documents {
                        let data = document.data()
                        
                        let programm = data["title"] as? String ?? ""
                        if self.tagView != nil {
                            self.tagView.textFont = UIFont.systemFont(ofSize: 10)
                            self.tagView.alignment = .left// possible values are .Left, .Center, and .Right
                            self.tagView.addTag("Industri \(programm)")
                            
                            self.perniagaan.append(programm)
                        }


                    }
                    if self.tagView != nil {
                                                self.tagView.addTag("Semua Industri")
                    }

                }
        }
        
    }
    

    @objc func rangeSliderValueChanged(_ rangeSlider: RangeSlider) {
        
        let roundedValue = (Int(rangeSlider1.upperValue) / step) * step
        rangeSlider1.upperValue = Double(roundedValue)
        let roundedValue2 = (Int(rangeSlider1.lowerValue) / step) * step
        rangeSlider1.lowerValue = Double(roundedValue2)
        minLabel.text = "RM \(Int(rangeSlider1.lowerValue))"
        maxLabel.text = "RM \(Int(rangeSlider1.upperValue))"
        print("Range slider value changed: (\(Int(rangeSlider1.lowerValue)) , \(Int(rangeSlider1.upperValue))")
        filterMin = Int(rangeSlider1.lowerValue)
        filterMax = Int(rangeSlider1.upperValue)
        print ("new filter min value is \(Int(filterMin ?? 0))")
        print ("new filter max value is \(Int(filterMax ))")
    }
    
    @objc func rangeSliderValueChanged2(_ rangeSlider: RangeSlider) {
        let roundedValue = (Int(rangeSlider2.upperValue) / step) * step
        rangeSlider2.upperValue = Double(roundedValue)
        let roundedValue2 = (Int(rangeSlider2.lowerValue) / step) * step
        rangeSlider2.lowerValue = Double(roundedValue2)
        minLabel2.text = "RM \(Int(rangeSlider2.lowerValue))"
        maxLabel2.text = "RM \(Int(rangeSlider2.upperValue))"
        print("Range slider value changed: (\(Int(rangeSlider2.lowerValue)) , \(Int(rangeSlider2.upperValue))")
        
    }
    
    func setupLeftNavItem(navigationItem: UINavigationItem) {
        let followButton = UIButton(type: .custom)
        followButton.setImage(#imageLiteral(resourceName: "menu_white").withRenderingMode(.alwaysTemplate), for: .normal)
        followButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        followButton.addTarget(self, action: #selector(menuTapped), for: .touchUpInside)
        followButton.tintColor = UIColor(white: 1, alpha: 1)
        
        //   navigationItem.leftBarButtonItem = UIBarButtonItem(customView: followButton)
        
        
        let menuBarItem = UIBarButtonItem(customView: followButton)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 30)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 30)
        currHeight?.isActive = true
        navigationItem.leftBarButtonItem = menuBarItem
        
        
        
    }
    
    @objc func menuTapped() {
        UserDefaults.standard.set(nil, forKey: "filter")
        Helper.helper.switchToNavigationViewController(Navigation: "homepage")
        print("menu did tapped")
        
    }
    
    func setupRightNavItems(navigationItem: UINavigationItem) {
        
        //MARK Right-left icon (filter)
        let searchButton = UIButton(type: .system)
        searchButton.setImage(#imageLiteral(resourceName: "filter1x").withRenderingMode(.alwaysTemplate), for: .normal)
        searchButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        searchButton.tintColor = UIColor(white: 1, alpha: 1)
        searchButton.addTarget(self, action: #selector(filterTapped), for: .touchUpInside)
        
        //MARK Right-right icon (Profile)
        let composeButton = UIButton(type: .system)
        composeButton.setImage(#imageLiteral(resourceName: "newmale1").withRenderingMode(.alwaysOriginal), for: .normal)
        composeButton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        composeButton.tintColor = UIColor.groupTableViewBackground
        composeButton.addTarget(self, action: #selector(profileTapped), for: .touchUpInside)
        
        
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: composeButton), UIBarButtonItem(customView: searchButton)]
    }
    
    @objc func profileTapped() {
        Helper.helper.switchToNavigationViewController(Navigation: "myprofilepage")
        print("profile did tapped")
        
    }
    
 
    
    @objc func toggleSideMenu() {
        if sideMenuOpen {
            sideMenuOpen = false
            multiplierCons.constant = -500
            setupRightNavItems(navigationItem: navigationItem)
            UIView.animate(withDuration: 0.2, delay: 0.2, options: .curveEaseOut,
                           animations: {self.detailsView.alpha = 0},
                           completion: { _ in self.detailsView.isHidden = true
                            //Do anything else that depends on this animation ending
            })
            
        } else {
            sideMenuOpen = true
            
            multiplierCons.constant = 0
            

            //MARK Right-left icon (filter)
            let searchButton = UIButton(type: .system)
            searchButton.setImage(#imageLiteral(resourceName: "filter1x").withRenderingMode(.alwaysTemplate), for: .normal)
            searchButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            searchButton.tintColor = UIColor(white: 1, alpha: 0.3)
            searchButton.addTarget(self, action: #selector(filterTapped), for: .touchUpInside)
            
            //MARK Right-right icon (Profile)
            let composeButton = UIButton(type: .system)
            composeButton.setImage(#imageLiteral(resourceName: "newmale1").withRenderingMode(.alwaysOriginal), for: .normal)
            composeButton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
//            composeButton.tintColor = UIColor.groupTableViewBackground
            composeButton.addTarget(self, action: #selector(profileTapped), for: .touchUpInside)
            
            
            navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: composeButton), UIBarButtonItem(customView: searchButton)]

            
            UIView.animate(withDuration: 0.0, delay: 0.0, options: .curveEaseOut,
                           animations: {self.detailsView.alpha = 1},
                           completion: { _ in self.detailsView.isHidden = false
                            //Do anything else that depends on this animation ending
            })
            
        }
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }

    
    var query = Firestore.firestore().collection("fund")
   

    @IBAction func okButtonDidTapped(_ sender: Any) {
        self.alertPopOut.isHidden = true
  Helper.helper.switchToNavigationViewController(Navigation: "businessprofilepage")
    }
    
    @IBOutlet weak var alertPopOut: UIView!
    
    @IBAction func filterButtonDidTapped(_ sender: Any) {
        self.agencyList.removeAll()
       
        let emailUser = user!.email
             UserDefaults.standard.set(nil, forKey: "filter")
        docRef = Firestore.firestore().document("User/\(emailUser ?? "")/Business Profile/\(emailUser ?? "")")
        
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                 Helper.helper.test(vc: self)
                let data = document.data()
                var bussinessProgram = data!["business_Type"] as? String ?? ""
                var bussinessIndustry = data!["industry_Category"] as? String ?? ""
                var bussinessSize = data!["industry_Size"] as? String ?? ""
                
                print (bussinessProgram)
                print (bussinessIndustry)
                print (bussinessSize)
                
                var newQuery: Query = self.query
                if bussinessProgram == "All" {
                    bussinessProgram = ""
                }
                if bussinessIndustry == "All" {
                    bussinessIndustry = ""
                }
                if bussinessSize == "All" {
                    bussinessSize = ""
                }
                
                if bussinessSize != "" {
                    newQuery = newQuery.whereField("perniagaan", isEqualTo: bussinessSize.capitalized )
                }
                if bussinessIndustry != "" {
                    newQuery = newQuery.whereField("industri", isEqualTo: bussinessIndustry.capitalized )
                }
                if bussinessProgram != "" {
                    newQuery =  newQuery.whereField("type", isEqualTo: bussinessProgram.capitalized)
                }
                
                newQuery.getDocuments()
                    {
                        (querySnapshot, err) in
                        
                        if let err = err
                        {
                            print("Error getting documents: \(err)");
                        }
                        else
                        {
                            //  var agencyNumber = 0
                            for document in querySnapshot!.documents {
                                let data = document.data()
                                let agencyPath = data["agensi"] as? String ?? ""
                                let title = data["title"] as? String ?? ""
                                let program = data["program"] as? String ?? ""
                                let perniagaan = data["perniagaan"] as? String ?? ""
                                let newMax = data["max"] as? Int
                                let agencyId = document.documentID
                                
                                //start
                                let query = Firestore.firestore().collection("Agensi")
                                let newQuery = query.whereField("name", isEqualTo: "\(agencyPath)")
                                
                                newQuery.getDocuments()
                                    {
                                        (querySnapshot, err) in
                                        
                                        if let err = err
                                        {
                                            print("Error getting documents: \(err)");
                                        }
                                        else
                                        {
                                            //  var agencyNumber = 0
                                            for document in querySnapshot!.documents {
                                                let data = document.data()
                                                let logo = data["logo"] as? String ?? ""
                                                
                                                let newModel = DisplayModel(agency: title, agencyId: agencyId, programTag: program, perniagaanTag: perniagaan, max: newMax, agencyPath: agencyPath, logoUrl: logo, agencyTitle: agencyPath)
                                                

                                                    self.agencyList.append(newModel)
                                              //  print (self.agencyList)
                                                self.tableView.reloadData()

                                                
                                            }
                                            
                                        }
                                }
                            }
                            
                            
                            NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
                        }
                }
                
                
            } else {
                
                self.dismiss(animated: false, completion: nil)
                self.alertPopOut.isHidden = false
                
            }
        }
        

   }
    

}

//MARK UITableViewDelegate
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        if UserDefaults.standard.string(forKey: "filter") == "search" {
            return filteredData.count
        } else {
            
            return agencyList.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      //  let cellData: DisplayModel
       
        if UserDefaults.standard.string(forKey: "filter") == "search" {
            let cellData: DisplayModel
            cellData = filteredData[indexPath.row]
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? HomeTableViewCell
            
            let tapGesture = UITapGestureRecognizer (target: self, action: #selector(imgTap(tapGesture:)))
            tapGesture.numberOfTapsRequired = 1
            cell?.agencyName.text = cellData.agency
            cell?.agencyImage.sd_setImage(with: URL(string: "\(cellData.logoUrl ?? "")"), placeholderImage: UIImage(named: "no_pic_image"))
            print (cellData.logoUrl ?? "")
            //cell?.agencyImage.image = UIImage(named: "Cradle")
            cell?.agencyImage.addGestureRecognizer(tapGesture)
            cell?.agencyImage.isUserInteractionEnabled = true
            cell?.agencyImage.tag = indexPath.row
            cell?.tagListView.removeAllTags()
            cell?.tagListView.textFont = UIFont.systemFont(ofSize: 10)
            cell?.tagListView.alignment = .left
            cell?.tagListView.addTag(cellData.programTag!.capitalized)
            cell?.tagListView.addTag("Industri \(cellData.perniagaanTag!.capitalized)")
            
            
            return cell!
            
        } else {
            let cellData: DisplayModel
            cellData = agencyList[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? HomeTableViewCell
            
            let tapGesture = UITapGestureRecognizer (target: self, action: #selector(imgTap(tapGesture:)))
            tapGesture.numberOfTapsRequired = 1
            cell?.agencyName.text = cellData.agency
            cell?.agencyImage.sd_setImage(with: URL(string: "\(cellData.logoUrl ?? "")"), placeholderImage: UIImage(named: "no_pic_image"))
            print (cellData.logoUrl ?? "")
            //cell?.agencyImage.image = UIImage(named: "Cradle")
            cell?.agencyImage.addGestureRecognizer(tapGesture)
            cell?.agencyImage.isUserInteractionEnabled = true
            cell?.agencyImage.tag = indexPath.row
            cell?.tagListView.removeAllTags()
            cell?.tagListView.textFont = UIFont.systemFont(ofSize: 10)
            cell?.tagListView.alignment = .left
            cell?.tagListView.addTag(cellData.programTag!.capitalized)
            cell?.tagListView.addTag("Industri \(cellData.perniagaanTag!.capitalized)")
            
          //  print ("the number data is: \(agencyList.count)")
            self.dismiss(animated: false, completion: nil)
//            if agencyList.count >= 400 {
//                self.dismiss(animated: false, completion: nil)
//            }
            
//
//            if agencyList.last != nil {
//
//                print("the last agency list is 2: \(String(describing: agencyList.last))")
//            } else {
//                print("the last agency list is: \(String(describing: agencyList.last))") }
            
            return cell!

        }


        

    }
    
    @objc func imgTap(tapGesture: UITapGestureRecognizer) {
        let imgView = tapGesture.view as! UIImageView
        print(imgView.tag)
        
        
        if UserDefaults.standard.string(forKey: "filter") == "search" {
            let cellData: DisplayModel
            cellData = filteredData[imgView.tag]
            
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desVC = mainStoryboard.instantiateViewController(withIdentifier: "detailagencypage") as! AgencyTableViewController
            desVC.agencyPath = cellData.agencyPath!
            self.navigationController?.pushViewController(desVC, animated: true)
            //let idToMove = imgView.tag
            if (imgView.tag == 0) {}
            else {}
        } else {
            let cellData: DisplayModel
            cellData = agencyList[imgView.tag]
            
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desVC = mainStoryboard.instantiateViewController(withIdentifier: "detailagencypage") as! AgencyTableViewController
            desVC.agencyPath = cellData.agencyPath!
            self.navigationController?.pushViewController(desVC, animated: true)
            //let idToMove = imgView.tag
            if (imgView.tag == 0) {}
            else {}
        }
    
       
        

        //Do further execution where you need idToMove
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didTapMessageAtIndexPath: \(indexPath.item)")
        print("didTapMessageAtIndexPath: \(indexPath.row)")
        
        
    
        if UserDefaults.standard.string(forKey: "filter") == "search"  {
            var cellData: DisplayModel
            cellData = filteredData[indexPath.row]
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desVC = mainStoryboard.instantiateViewController(withIdentifier: "detailprogrampage") as! ProgramTableViewController
            desVC.agencyId = cellData.agencyId!
            self.navigationController?.pushViewController(desVC, animated: true)
        } else {
            var cellData: DisplayModel
            cellData = agencyList[indexPath.row]
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desVC = mainStoryboard.instantiateViewController(withIdentifier: "detailprogrampage") as! ProgramTableViewController
            desVC.agencyId = cellData.agencyId!
            self.navigationController?.pushViewController(desVC, animated: true)
        }
        

        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        //
        //            if (filterProgram != "" || filterPerniagaan != "" || filterIndustri != "" || filterAgencyType != "" || filterMin != nil || filterMax != 1500000 )  {
        //
        //                let lastSectionIndex = tableView.numberOfSections - 1
        //                let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        //
        //                if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
        //
        //                    nextFilter()
        //                    // print("this is the last cell")
        //                    let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        //                    spinner.startAnimating()
        //                    spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
        //
        //                    tableView.tableFooterView = spinner
        //                    tableView.tableFooterView?.isHidden = false
        //                }
        //
        //
        //                //  }
        //                //self.dismiss(animated: false, completion: nil)
        //            } else {
        ////                let lastElement = agencyList.count - 1
        ////                if indexPath.row == lastElement {
        //
        //                    let lastSectionIndex = tableView.numberOfSections - 1
        //                    let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        //
        //                    if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
        //
        //               //         getMore()
        //                        // print("this is the last cell")
        //                        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        //                        spinner.startAnimating()
        //                        spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
        //
        //                        tableView.tableFooterView = spinner
        //                        tableView.tableFooterView?.isHidden = false
        //                    }
        //
        //
        //              //  }
        //        }
        
    }
    
}

//MARK UISearchBarDelegate
extension HomeViewController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        UserDefaults.standard.set(nil, forKey: "filter")
        searchController.searchBar.text = ""
        searchController.searchBar.showsCancelButton = false
        self.tableView.tableHeaderView = nil;
        self.tableView.tableFooterView = nil;
        
        tableView.reloadData()
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
//        filteredData = agencyList.filter {
//            $0.agency!.localizedCaseInsensitiveContains(searchText)
//                || ($0.programTag?.localizedCaseInsensitiveContains(searchText))!
//                || ($0.perniagaanTag?.localizedCaseInsensitiveContains(searchText))!
//                || ($0.agencyTitle?.localizedCaseInsensitiveContains(searchText))!
//        }
//        let searchString = searchController.searchBar.text
//        newSearchString = searchString!
//
//        tableView.reloadData()
    }
    
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("search button click")
         UserDefaults.standard.set("search", forKey: "filter")
        let searchString = searchController.searchBar.text
        filteredData = agencyList.filter {
            $0.agency!.localizedCaseInsensitiveContains(searchString!)
                || ($0.programTag?.localizedCaseInsensitiveContains(searchString!))!
                || ($0.perniagaanTag?.localizedCaseInsensitiveContains(searchString!))!
                || ($0.agencyTitle?.localizedCaseInsensitiveContains(searchString!))!
        }
        
        newSearchString = searchString!
        
        tableView.reloadData()
        
        self.tableView.tableHeaderView = headerView
        self.tableView.tableFooterView = footerView
        
        searchResultLabel.text = ("\"\(newSearchString)\"")
        
    }
    
}

//MARK TagListViewDelegate
extension HomeViewController: TagListViewDelegate {
    
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title), \(sender)")
        
        
        sender.tagViews.forEach { (tag) in
            tag.isSelected = false
            let newTitle = title.dropFirst(9)
            if title == "Semua Industri" {
                self.filterPerniagaan = ""
            } else {
                self.filterPerniagaan = String(newTitle.capitalized)
             //   print("filterPerniagaan id: \(self.filterPerniagaan)")
            }
            tag.textColor = UIColor.white
            
        }
        
        tagView.isSelected = !tagView.isSelected
        
    }
}

//MARK UIPickerDelegate
extension HomeViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    func agencyPicker(pickername: UITextField) {
        
        let dayPicker = UIPickerView()
        dayPicker.tag = 1
        dayPicker.delegate = self
        
        pickername.inputView = dayPicker
        
        //Customizations
        dayPicker.backgroundColor = .white
    }
    
    func industryPicker(pickername: UITextField) {
        
        let dayPicker = UIPickerView()
        dayPicker.tag = 2
        dayPicker.delegate = self
        
        pickername.inputView = dayPicker
        
        //Customizations
        dayPicker.backgroundColor = .white
    }
    
    func programPicker(pickername: UITextField) {
        
        let dayPicker = UIPickerView()
        dayPicker.tag = 3
        dayPicker.delegate = self
        pickername.inputView = dayPicker
        
        //Customizations
        dayPicker.backgroundColor = .white
    }
    
    func createToolbar(pickername: UITextField) {
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        //Customizations
        toolBar.barTintColor = .orange
        toolBar.tintColor = .white
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(HomeViewController.dismissKeyboard))
        
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        pickername.inputAccessoryView = toolBar
    }
    
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if (pickerView.tag == 1){
            return agency.count
        }else if (pickerView.tag == 2){
            return industry.count
        } else {
            return program.count
        }
        
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if (pickerView.tag == 1){
            return agency[row]
        }else if (pickerView.tag == 2){
            return industry[row]
        } else {
            return program[row]
        }
        
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if (pickerView.tag == 1){
            selectedDay = agency[row]
            agencyCategoryDropDown.text = selectedDay
            
            if agencyCategoryDropDown.text == "All" {
                selectedDay = ""
                self.filterAgencyType = selectedDay!.capitalized
                print("filterAgencyType is: \(self.filterAgencyType)")

            } else {
                self.filterAgencyType = selectedDay!.capitalized
                print("filterAgencyType is: \(self.filterAgencyType)")
            }


            
        } else if (pickerView.tag == 2){
            selectedDay = industry[row]
            industryCategoryDropDown.text = selectedDay
            
            if industryCategoryDropDown.text == "All" {
                selectedDay = ""
                self.filterIndustri = selectedDay!.capitalized
                print("filterIndustri is: \(self.filterIndustri)")
                
            } else {
                self.filterIndustri = selectedDay!.capitalized
                print("filterIndustri is: \(self.filterIndustri)")
            }

        } else {
            selectedDay = program[row]
            programCategoryDropDown.text = selectedDay
            
            if programCategoryDropDown.text == "All" {
                selectedDay = ""
                 self.filterProgram = selectedDay!.capitalized
                print("filterProgram is: \( self.filterProgram)")
                
            } else {
                self.filterProgram = selectedDay!.capitalized
                print("filterProgram is: \( self.filterProgram)")
            }

        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var label: UILabel
        
        if let view = view as? UILabel {
            label = view
        } else {
            label = UILabel()
        }
        
        label.textColor = .orange
        label.textAlignment = .center
        label.font = UIFont(name: "Montserrat", size: 30)
        
        
        if (pickerView.tag == 1){
            label.text = agency[row]
        } else if (pickerView.tag == 2){
            label.text = industry[row]
        } else {
            label.text = program[row]
        }
        
        return label
    }
}

extension UIView{
    func showBlurLoader(){
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityIndicator.startAnimating()
        
        blurEffectView.contentView.addSubview(activityIndicator)
        activityIndicator.center = blurEffectView.contentView.center
        
        self.addSubview(blurEffectView)
    }
    
    func removeBluerLoader(){
        self.subviews.flatMap {  $0 as? UIVisualEffectView }.forEach {
            $0.removeFromSuperview()
        }
    }
}

class SearchBarContainerView: UIView {
    
    let searchBar: UISearchBar
    
    init(customSearchBar: UISearchBar) {
        searchBar = customSearchBar
        super.init(frame: CGRect.zero)
        
        addSubview(searchBar)
    }
    
    override convenience init(frame: CGRect) {
        self.init(customSearchBar: UISearchBar())
        self.frame = frame
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        searchBar.frame = bounds
    }
}

