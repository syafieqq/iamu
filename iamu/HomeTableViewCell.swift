//
//  HomeTableViewCell.swift
//  iamu
//
//  Created by Muhammad Iqbal on 14/04/2018.
//  Copyright © 2018 Muhammad Iqbal. All rights reserved.
//

import UIKit
import TagListView
class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var agencyName: UILabel!
    @IBOutlet weak var agencyImage: UIImageView!
    @IBOutlet weak var tagListView: TagListView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        cellView.layer.cornerRadius = 6
//         cellView.layer.masksToBounds = false
//        cellView.clipsToBounds = true
//
//        firstView.layer.cornerRadius = 6
//        firstView.layer.masksToBounds = false
//        firstView.clipsToBounds = true
//
//        secondView.layer.cornerRadius = 6
//        secondView.layer.masksToBounds = false
//        secondView.clipsToBounds = true
//        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
