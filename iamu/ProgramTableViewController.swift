//
//  ProgramTableViewController.swift
//  iamu
//
//  Created by Muhammad Iqbal on 25/04/2018.
//  Copyright © 2018 Muhammad Iqbal. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import FirebaseFirestore
import TagListView
class ProgramTableViewController: UITableViewController {
    
//var sizeLabel1 = 1
// var sizeLabel2 = 1
    var agencyId = ""
  //  var descriptionn: UILabel
    var linkUrl = ""
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var qualificationLabel: UILabel!
    @IBOutlet weak var minLabel: UILabel!
    @IBOutlet weak var maxLabel: UILabel!
    @IBOutlet weak var trainingLabel: UILabel!
    @IBOutlet weak var agencyNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var telLabel: UILabel!
    @IBOutlet weak var faxLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    @IBOutlet weak var tagListView: TagListView!
    @IBOutlet weak var companyImage: UIImageView!
    
    var heightOfDescription: CGFloat = 1
    var heightOfKelayakan: CGFloat = 1
    var heightOfAddress: CGFloat = 1
    var heightOfAddress2: CGFloat = 1
    var heightOfname: CGFloat = 1
    var heightOftel: CGFloat = 1
    var heightOffax: CGFloat = 1
    var heightOfWebsite: CGFloat = 1
  
    var agencyPath = ""
    
    var docRef : DocumentReference!
    let user = Auth.auth().currentUser
    
    @IBAction func openLinkDidTapped(_ sender: Any) {
        
        print (linkUrl)
        
        if let url = URL(string: "\(linkUrl)") {
            if #available(iOS 10, *){
                UIApplication.shared.open(url)
            }else{
                UIApplication.shared.openURL(url)
            }
            
        }
    }
    
    @IBAction func buttonImageAgencyDidTapped(_ sender: Any) {
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let desVC = mainStoryboard.instantiateViewController(withIdentifier: "detailagencypage") as! AgencyTableViewController
        desVC.agencyPath = agencyPath
        self.navigationController?.pushViewController(desVC, animated: true)
        
    }
    
    @IBAction func agencyNameAsButtonDidTapped(_ sender: Any) {
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let desVC = mainStoryboard.instantiateViewController(withIdentifier: "detailagencypage") as! AgencyTableViewController
        desVC.agencyPath = agencyPath
        self.navigationController?.pushViewController(desVC, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
                getData()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
//        self.navigationController?.navigationBar.barTintColor = UIColor.white
        print("label height 1 is \(descriptionLabel.bounds.size.height)")
        
        self.navigationItem.title = "Program"
    }
    
    func estimatedHeightOfLabel(text: String) -> CGFloat {
        
        let size = CGSize(width: view.frame.width - 16, height: 1000)
        
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        
        let attributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12)]
        
        let rectangleHeight = String(text).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
        
        return rectangleHeight
    }
    
    
    @objc func tappedView(tapGesture: UITapGestureRecognizer) {
        print("image tapped")
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let desVC = mainStoryboard.instantiateViewController(withIdentifier: "detailagencypage") as! AgencyTableViewController
        desVC.agencyPath = agencyPath
        self.navigationController?.pushViewController(desVC, animated: true)
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        //tableView.reloadData()
       // Helper.helper.test(vc: self)
        let tapGesture = UITapGestureRecognizer (target: self, action: #selector(tappedView(tapGesture:)))
        tapGesture.numberOfTapsRequired = 1
       companyImage.addGestureRecognizer(tapGesture)
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 1, green: 0.56, blue: 0.27, alpha: 1)

        docRef = Firestore.firestore().document("fund/\(agencyId)")
        print("my agency id is \(agencyId)")


    }
    
    
   override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//    let font = UIFont(name: "Roboto", size: 12.0)
//    let heighth = self.descriptionLabel.text?.heightWithConstrainedWidth(width: UIScreen.main.bounds.width - 64 , font: font!)
//    let heighth2 = self.qualificationLabel.text?.heightWithConstrainedWidth(width: UIScreen.main.bounds.width - 64 , font: font!)
//    let heighth3 = self.addressLabel.text?.heightWithConstrainedWidth(width: UIScreen.main.bounds.width - 64 , font: font!)
//    let heighth4 = self.trainingLabel.text?.heightWithConstrainedWidth(width: UIScreen.main.bounds.width - 64 , font: font!)
//    let heighth5 = self.nameLabel.text?.heightWithConstrainedWidth(width: UIScreen.main.bounds.width - 64 , font: font!)
//    let heighth6 = self.telLabel.text?.heightWithConstrainedWidth(width: UIScreen.main.bounds.width - 64 , font: font!)
//    let heighth7 = self.faxLabel.text?.heightWithConstrainedWidth(width: UIScreen.main.bounds.width - 64 , font: font!)
    
//    self.heightOfDescription = heighth ?? 10
//    self.heightOfKelayakan = heighth2 ?? 10
//    self.heightOfAddress = heighth3 ?? 10
//    self.heightOfAddress2 = heighth4 ?? 10
//    self.heightOfname = heighth5 ?? 10
//    self.heightOftel = heighth6 ?? 10
//    self.heightOffax = heighth7 ?? 10
    
    print ("kelayakan height is \(heightOfKelayakan)")
     print ("description height is \(heightOfDescription)")
     print ("address height is \(heightOfAddress)")
     print ("address2 height is \(heightOfAddress2)")
     print ("tel height is \(heightOftel)")
    print ("fax height is \(heightOffax)")
    
    return (850 + heightOfDescription + heightOfKelayakan + heightOfAddress + heightOfAddress2 + heightOfname + heightOftel + heightOffax + heightOfWebsite)
    }


    
    func getData () {
        
        docRef.getDocument { (docSnapshot, error) in
            guard let docSnapshot = docSnapshot, docSnapshot.exists else { return }
            let data = docSnapshot.data()
            
            let name = data!["title"] as? String ?? ""
            let description = data!["description"] as? String ?? ""
            let qualification = data!["qualification"] as? String ?? ""
            let min = data!["min"] as! Int
            let max = data!["max"] as! Int
            let agencyData = data!["agensi"] as? String ?? ""
            let website = data!["website"] as? String ?? ""
            let program = data!["program"] as? String ?? ""
            let perniagaan = data!["perniagaan"] as? String ?? ""
            let industri = data!["industri"] as? String ?? ""
           
            self.agencyPath = agencyData
            
            
            //MARK add tag
            let query = Firestore.firestore().collection("Agensi")
            let newQuery = query.whereField("name", isEqualTo: "\(agencyData)")
            let font = UIFont(name: "Roboto", size: 12.0)
            newQuery.getDocuments()
                {
                    (querySnapshot, err) in
                    
                    if let err = err
                    {
                        print("Error getting documents: \(err)");
                    }
                    else
                    {
                        //  var agencyNumber = 0
                        for document in querySnapshot!.documents {
                            let data = document.data()
                            let logo = data["logo"] as? String ?? ""

                            self.companyImage.sd_setImage(with: URL(string: "\(logo)"), placeholderImage: UIImage(named: "no_pic_image"))
                            
//                            print("new perniagaan is \(perniagaan)")
//                            print("new program is \(program)")
//                            print("new industri is \(industri)")

                            let training = data["address"] as? String ?? ""
                            let address = data["address"] as? String ?? ""
                            let tel = data["tel"]
                            let fax = data["fax"]
                            let email = data["email"] as? String ?? ""
   

                            self.trainingLabel.text! = training
                            self.addressLabel.text! = address
                            self.telLabel.text! = "\(tel ?? "")"
                            self.faxLabel.text! = "\(fax ?? "")"
                            self.emailLabel.text! = email
                            
                            let heighth3 = self.addressLabel.text?.heightWithConstrainedWidth(width: UIScreen.main.bounds.width - 64 , font: font!)
                            let heighth4 = self.trainingLabel.text?.heightWithConstrainedWidth(width: UIScreen.main.bounds.width - 64 , font: font!)
                            let heighth6 = self.telLabel.text?.heightWithConstrainedWidth(width: UIScreen.main.bounds.width - 64 , font: font!)
                            let heighth7 = self.faxLabel.text?.heightWithConstrainedWidth(width: UIScreen.main.bounds.width - 64 , font: font!)
                            
                            self.heightOftel = heighth6 ?? 10
                            self.heightOffax = heighth7 ?? 10
                            self.heightOfAddress = heighth3 ?? 10
                            self.heightOfAddress2 = heighth4 ?? 10
                            
                            print ("heighthaddr \(heighth3 ?? 10)")
                            print ("heighthaddr2 \(heighth4 ?? 10)")
                            print ("heighthtel \(heighth6 ?? 10)")
                            print ("heighthax \(heighth7 ?? 10)")
                            self.tableView.reloadData()




                        }
                        
                    }
            }
            
            

            
            self.tagListView.removeAllTags()
            self.tagListView.textFont = UIFont.systemFont(ofSize: 12)
            self.tagListView.alignment = .center // possible values
            self.tagListView.addTag(program.capitalized)
            self.tagListView.addTag(industri.capitalized)
            self.tagListView.addTag("\(perniagaan.capitalized)")
            self.linkUrl = website
            self.websiteLabel.text! = website
            self.agencyNameLabel.text! = agencyData
            self.nameLabel.text! = name
            self.descriptionLabel.text! = description
            self.qualificationLabel.text! = qualification
            self.minLabel.text! = "RM \(String(min))"
            self.maxLabel.text! = "RM \(String(max))"
            

            let heighth = self.descriptionLabel.text?.heightWithConstrainedWidth(width: UIScreen.main.bounds.width - 64 , font: font!)
            let heighth2 = self.qualificationLabel.text?.heightWithConstrainedWidth(width: UIScreen.main.bounds.width - 64 , font: font!)
            let heighth5 = self.nameLabel.text?.heightWithConstrainedWidth(width: UIScreen.main.bounds.width - 64 , font: font!)
            let heighth10 = self.websiteLabel.text?.heightWithConstrainedWidth(width: UIScreen.main.bounds.width - 64 , font: font!)

            
            self.heightOfDescription = heighth ?? 10
            self.heightOfKelayakan = heighth2 ?? 10
            self.heightOfname = heighth5 ?? 10
            self.heightOfWebsite = heighth10 ?? 10


            
            print ("heighthdesc \(heighth ?? 10)")
            print ("heighthklykn \(heighth2 ?? 10)")
            print ("heighthname \(heighth5 ?? 10)")
            print ("heightwebsite \(heighth10 ?? 10)")

            print (UIScreen.main.bounds.width - 64)
 

           // print ("frame.height\(self.descriptionLabel.frame.height)")
            
            self.tableView.reloadData()
           self.dismiss(animated: false, completion: nil)
    
            
        }
        tableView.reloadData()
        //self.dismiss(animated: false, completion: nil)
        
    }


}
extension String {
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedStringKey.font: font], context: nil)
        return boundingBox.height
    }
}

