//
//  LoadingView.swift
//  iamu
//
//  Created by Muhammad Iqbal on 03/06/2018.
//  Copyright © 2018 Muhammad Iqbal. All rights reserved.
//

import Foundation
import UIKit
class LoadingView {
    
    let uiView          :   UIView
  //  let message         :   String
    //let messageLabel    =   UILabel()
    
    let loadingSV       =   UIView()
    let loadingView     =   UIView()
    let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    
    init(uiView: UIView) {
        self.uiView     =   uiView
      //  self.message    =   message
        self.setup()
    }
    
    func setup(){
        _   = uiView.bounds.width
        _  = uiView.bounds.height
        
        // Configuring the message label
//        messageLabel.text             = message
//        messageLabel.textColor        = UIColor.darkGray
//        messageLabel.textAlignment    = .center
//        messageLabel.numberOfLines    = 3
//        messageLabel.lineBreakMode    = .byWordWrapping
        
        // Creating stackView to center and align Label and Activity Indicator
       // loadingSV.axis          = .vertical
//        loadingSV.distribution  = .equalSpacing
//        loadingSV.alignment     = .center
//        loadingSV.addArrangedSubview(activityIndicator)
      //  loadingSV.addArrangedSubview(messageLabel)
        
        // Creating loadingView, this acts as a background for label and activityIndicator
        loadingView.frame           = uiView.frame
        loadingView.center          = uiView.center
        loadingView.backgroundColor = UIColor.darkGray.withAlphaComponent(0.8)
        loadingView.clipsToBounds   = true
        
        // Disabling auto constraints
        loadingSV.translatesAutoresizingMaskIntoConstraints = false
        
        // Adding subviews
        loadingView.addSubview(loadingSV)
        uiView.addSubview(loadingView)
        activityIndicator.startAnimating()
        
        // Views dictionary
//        let views = [
//            "loadingSV": loadingSV
//        ]
        
        // Constraints for loadingSV
//        uiView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[loadingSV(300)]-|", options: [], metrics: nil, views: views))
//        uiView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(viewHeight / 3)-[loadingSV(50)]-|", options: [], metrics: nil, views: views))
    }
    
    // Call this method to hide loadingView
    func show() {
        loadingView.isHidden = false
    }
    
    // Call this method to show loadingView
    func hide(){
        loadingView.isHidden = true
    }
    
    // Call this method to check if loading view already exists
    func isHidden() -> Bool{
        if loadingView.isHidden == false{
            return false
        }
        else{
            return true
        }
    }
}
