//
//  UISwitchExt.swift
//  iamu
//
//  Created by Close Loop on 07/06/2018.
//  Copyright © 2018 Muhammad Iqbal. All rights reserved.
//

import UIKit
@IBDesignable

class UISwitchExt : UISwitch {
    @IBInspectable var offTint : UIColor? {
        didSet {
            self.tintColor = offTint
            self.layer.cornerRadius = 16
            self.backgroundColor = offTint;
//            self.tintColor = UIColor.gray;
        }
    }
}

